function TraceRoute(map, hostname, index, aside, dvalue, evalue, chart) {

    let mThis = this;

    /* Seul de lissage */
    let SEUIL = 1;

    /* Variables - Constructeur */
    this.map = map;
    this.hostname = hostname;
    this.index = index;
    this.marker = [null, null]; /* Marqueur de dÃ©but de parcours */
    this.markerTrack; /* Marqueur de suivi de la trace */
    this.visible = true;

    this.aside = aside;
    this.dvalue = dvalue;
    this.evalue = evalue;

    /* Variable TracÃ© */
    this.color = ['#0000FF', '#FF0000', '#008000', '#FFD326', '#FF7F00', '#582900', '#FD6C9E', '#9C2BCB', '#7B7B7B', '#3D3D3D'];
    this.polyline = L.polyline([], {color: this.color[index]}).addTo(this.map);
    this.distance = [];
    this.elevation = [];

    /* Variable Point */
    this.point = [];

    this.dplus = 0;
    this.active = 0;

     var greenIcon = new L.Icon({
        iconUrl: 'images/marker-start.png',
        shadowUrl: 'images/marker-shadow.png',
        iconSize: [25, 41],
        iconAnchor: [12, 41],
        popupAnchor: [1, -34],
        shadowSize: [41, 41]
    });

    var redIcon = new L.Icon({
        iconUrl: 'images/marker-finish.png',
        shadowUrl: 'images/marker-shadow.png',
        iconSize: [25, 41],
        iconAnchor: [12, 41],
        popupAnchor: [1, -34],
        shadowSize: [41, 41]
    });

    /* Mise Ã  jour de la liste de trace */
    this.updateInfo = function(del) {
        let current = document.getElementById('trace_' + index);

        if(current != null) {
            let distance = current.getElementsByClassName('distance')[0].innerHTML = this.getDistance() + ' km';

            if(del && index != 0)
                current.remove();
        }

        if(this.evalue != null)
            this.evalue.innerHTML = this.getElevation();

        if(this.dvalue != null)
            this.dvalue.innerHTML = this.getDistance();

        let chartData = [];
        for(let i = 0, l = this.distance.length ; i < l ; i++)
            chartData.push([this.distance[i], this.elevation[i]]);

        /* Update chart */
        chart.update({
            series: [{
                data: chartData
            }],
        });

        let newMin = chart.yAxis[0].getExtremes().dataMin - 5;
        let newMax = chart.yAxis[0].getExtremes().dataMax + 5;

        chart.yAxis[0].setExtremes(newMin, newMax, true, false);
    }

    /* RÃ©cupÃ¨re la distance */
    this.getDistance = function() {
        distance = 0;

        path = this.polyline.getLatLngs();

        for(i = 0 ; i < path.length - 1 ; i++)
            distance += GEO_Distance(path[i], path[i+1]);

        return Math.round(distance * 100) / 100;
    }

    /* RÃ©cupÃ¨re le dÃ©nivelÃ© */
    this.getElevation = function() {
        this.dplus = 0;

        let mg = [];

        /*for(let i = 0, l = this.elevation.length ; i < l - 1; i++) {
            if(this.elevation[i+1] > this.elevation[i] + SEUIL)
                this.dplus += this.elevation[i+1] - this.elevation[i];
        }*/
        
        for(let i = 1, l = this.elevation.length ; i < l - 2 ; i++) {
            let v = (this.elevation[i-1] + this.elevation[i] + this.elevation[i+1]) / 3
            mg.push(v);
        }

        for(let i = 0, l = mg.length ; i < l - 1 ; i++) {
            if(mg[i+1] > mg[i] + SEUIL)
                this.dplus += mg[i+1] - mg[i];
        }

        return parseInt(this.dplus);
    }


    /* Reset du tracÃ© */
    this.reset = function() {
        var path = this.polyline.getLatLngs();
        var len = path.length;

        if(len > 0) {
            /* Suppression des points */
            for(var i = 0 ; i < len ; i++)
                path.pop();
    
            /* Suppression du marqueur */
            this.map.removeLayer(this.marker[0]);
    
            this.point = [];
    
            this.polyline.setLatLngs(path);

            this.distance = [];
            this.elevation = [];
        }

        this.updateInfo(true);
    }

    /* Affiche le tracÃ© */
    this.show = function() {
        let current = document.getElementById('trace_' + this.index);
        let visibility = current.getElementsByClassName('visibility')[0];

        visibility.src = 'images/visibility-on.png';

        this.map.addLayer(this.polyline);

        if(this.polyline.getLatLngs().length > 0)
            this.marker[0] = L.marker(this.polyline.getLatLngs()[0], {icon: greenIcon}).addTo(this.map);

        this.visible = true;
    }

    /* Masque le tracÃ© */
    this.hide = function() {
        let current = document.getElementById('trace_' + this.index);
        let visibility = current.getElementsByClassName('visibility')[0];

        visibility.src = 'images/visibility-off.png';
        
        this.map.removeLayer(this.polyline);

        if(this.marker[0] != undefined)
            this.map.removeLayer(this.marker[0]);

        this.visible = false;

    }

    /* Initialise la liste de trace */
    let iindex = this.index + 1;
    let p = document.createElement('p');
    let radio = document.createElement('input');
    let title = document.createElement('span');
    let span = document.createElement('span');
    let dist = document.createElement('span');
    let visibility = document.createElement('img');
    let img;

    p.id = 'trace_' + this.index;
    p.className = 'trace';

    radio.setAttribute('type', 'radio');
    radio.name = 'trace';
    radio.style.marginRight = '10px';
    radio.checked = 'true';

    title.innerHTML = 'Trace ' + iindex;

    radio.addEventListener('click', function() {
        mThis.active = Date.now();

        mThis.updateInfo(false);
    });

    span.innerHTML = ' - ';

    dist.className = 'distance';
    dist.innerHTML = this.getDistance() + ' km';

    title.style.color = this.color[index];
    span.style.color = this.color[index];
    dist.style.color = this.color[index];

    visibility.className = 'visibility';
    visibility.src = 'images/visibility-on.png';
    visibility.style.marginLeft = '8px';
    visibility.style.verticalAlign = 'middle';

    visibility.addEventListener('click', function() {
        if(mThis.visible)
            mThis.hide();
        else
            mThis.show();
    });

    if(this.aside != undefined) {
        p.appendChild(radio);
        p.appendChild(title);
        p.appendChild(span);
        p.appendChild(dist);
        p.appendChild(visibility);
        this.aside.appendChild(p);
    }

    this.active = Date.now();

    this.updateInfo(false);
}
