

function MAP_LoadLocalGPX(map, trace, gpx, loading) {
    var fr = new FileReader();

    fr.onload = function(e) {

	    var points = [];
	    var bounds = new L.LatLngBounds ();

        var parser = new DOMParser();
        var xml = parser.parseFromString(e.target.result, "text/xml");

        let trkpt = xml.getElementsByTagName('trkpt');
        let p = [], ele = [];

        for(let i = 0, l = trkpt.length ; i < l ; i++) {
            var lat = trkpt[i].getAttribute('lat');
            var lon = trkpt[i].getAttribute('lon');

            if(lat != null && lon != null) {
                ez = trkpt[i].getElementsByTagName('ele')[0];

                if(ez != null)
                    ele.push(Math.round(ez.textContent));

                pt = new L.LatLng(lat, lon);
                p.push(pt);

                bounds.extend(pt);
            }
        }

        trace.addLatLngs(p, ele);

        map.fitBounds(bounds);
    };

    fr.readAsText(gpx.files[0]);
}

function GEO_Deg2Rad(deg) {
    return deg * Math.PI / 180;
}

function GEO_Distance(p1, p2) {
    if((p1.lat == p2.lat) && (p1.lng == p2.lng))
        return 0;

    let ra = 6378.1370;
    let rp = 6356.7523;
    let rm = (2 * ra + rp) / 3;

    let theta = p2.lng - p1.lng;
    let distance = Math.acos(Math.sin(GEO_Deg2Rad(p1.lat)) * Math.sin(GEO_Deg2Rad(p2.lat)) + Math.cos(GEO_Deg2Rad(p1.lat)) * Math.cos(GEO_Deg2Rad(p2.lat)) * Math.cos(GEO_Deg2Rad(theta)));

    return distance * rm;
}