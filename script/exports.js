function exports(path, elevation) {
    let i = 0;

    data =
'<?xml version="1.0" encoding="utf-8"?>\n\
<gpx>\n\
  <metadata />\n\
  <trk>\n\
    <name>Course Ã  pied</name>\n\
    <type>running</type>\n\
    <trkseg>\n';

    path.forEach(function(item) {
        data += '      <trkpt lat="' + item.lat + '" lon="' + item.lng + '">\n\
        <ele>' + elevation[i++] + '</ele>\n\
      </trkpt>\n';
    });
    
    data +=
    '    </trkseg>\n\
  </trk>\n\
</gpx>';

    return data;
}

function utf8_to_b64(str) {
    return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, function(match, p1) {
        return String.fromCharCode('0x' + p1);
    }));
}
