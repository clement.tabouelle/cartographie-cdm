let layers = [];
let overlays = [];

/* OpenStreetMap */
layers['OpenStreetMap'] = L.tileLayer('https://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    attribution: '<a href="https://www.openstreetmap.fr/" target="_blank">Openstreetmap</a>',
    maxZoom: 18
});

/* OpenStreetMap France */
layers['OpenStreetMap France'] = L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
    attribution: '<a href="https://www.openstreetmap.fr/" target="_blank">Openstreetmap</a>',
    maxZoom: 18
});

/* OpenStreetMap Allemagne */
layers['OpenStreetMap Allemagne'] = L.tileLayer('https://{s}.tile.openstreetmap.de/tiles/osmde/{z}/{x}/{y}.png', {
    attribution: '<a href="https://www.openstreetmap.de/" target="_blank">Openstreetmap</a>',
    maxZoom: 17
});

/* OpenStreetMap Hot */
layers['OpenStreetMap Hot'] = L.tileLayer('https://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png', {
    attribution: '<a href="https://www.openstreetmap.fr/" target="_blank">Openstreetmap</a>',
    maxZoom: 20
});

/* OpenRiverboatMap */
layers['OpenRiverboatMap'] = L.tileLayer('https://{s}.tile.openstreetmap.fr/openriverboatmap/{z}/{x}/{y}.png', {
    attribution: '<a href="https://www.openstreetmap.fr/" target="_blank">Openstreetmap</a>',
    maxZoom: 20
});

/* OpenTopoMap */
layers['OpenTopoMap'] = L.tileLayer('https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png', {
    attribution: '<a href="https://opentopomap.org/" target="_blank">OpentopoMap</a>',
    maxZoom: 17
});

/* OpenCycleMap */
layers['OpenCycleMap'] = L.tileLayer('https://{s}.tile.thunderforest.com/cycle/{z}/{x}/{y}.png?apikey=' + TDF_API_KEY, {
    attribution: '<a href="https://www.thunderforest.com/" target="_blank">Thunderforest</a>',
    maxZoom: 20
});

/* OpenTransportMap */
layers['OpenTransportMap'] = L.tileLayer('https://{s}.tile.thunderforest.com/transport/{z}/{x}/{y}.png?apikey=' + TDF_API_KEY, {
    attribution: '<a href="https://www.thunderforest.com/" target="_blank">Thunderforest</a>',
    maxZoom: 20
});

/* OpenTransportMap Dark */
layers['OpenTransportMap Dark'] = L.tileLayer('https://{s}.tile.thunderforest.com/transport-dark/{z}/{x}/{y}.png?apikey=' + TDF_API_KEY, {
    attribution: '<a href="https://www.thunderforest.com/" target="_blank">Thunderforest</a>',
    maxZoom: 20
});

/* OpenLandscapeMap */
layers['OpenLandscapeMap'] = L.tileLayer('https://{s}.tile.thunderforest.com/landscape/{z}/{x}/{y}.png?apikey=' + TDF_API_KEY, {
    attribution: '<a href="https://www.thunderforest.com/" target="_blank">Thunderforest</a>',
    maxZoom: 20
});

/* OpenOutdoorsMap */
layers['OpenOutdoorsMap'] = L.tileLayer('https://{s}.tile.thunderforest.com/outdoors/{z}/{x}/{y}.png?apikey=' + TDF_API_KEY, {
    attribution: '<a href="https://www.thunderforest.com/" target="_blank">Thunderforest</a>',
    maxZoom: 20
});

/* OpenSpinalMap */
layers['OpenSpinalMap'] = L.tileLayer('https://{s}.tile.thunderforest.com/spinal-map/{z}/{x}/{y}.png?apikey=' + TDF_API_KEY, {
    attribution: '<a href="https://www.thunderforest.com/" target="_blank">Thunderforest</a>',
    maxZoom: 20
});

/* OpenPioneerMap */
layers['OpenPioneerMap'] = L.tileLayer('https://{s}.tile.thunderforest.com/pioneer/{z}/{x}/{y}.png?apikey=' + TDF_API_KEY, {
    attribution: '<a href="https://www.thunderforest.com/" target="_blank">Thunderforest</a>',
    maxZoom: 20
});

/* OpenMobileAtlasMap */
layers['OpenMobileAtlasMap'] = L.tileLayer('https://{s}.tile.thunderforest.com/mobile-atlas/{z}/{x}/{y}.png?apikey=' + TDF_API_KEY, {
    attribution: '<a href="https://www.thunderforest.com/" target="_blank">Thunderforest</a>',
    maxZoom: 20
});

/* OpenNeighbourhoodMap */
layers['OpenNeighbourhoodMap'] = L.tileLayer('https://{s}.tile.thunderforest.com/neighbourhood/{z}/{x}/{y}.png?apikey=' + TDF_API_KEY, {
    attribution: '<a href="https://www.thunderforest.com/" target="_blank">Thunderforest</a>',
    maxZoom: 20
});

/* Mapbox - Street */
layers['Mapbox Street'] = L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/{style}/tiles/{z}/{x}/{y}?access_token=' + BOX_API_KEY, {
    attribution: '<a href="https://www.mapbox.com/" target="_blank">Mapbox</a>',
    style: 'streets-v9',
    minZoom: 1,
    maxZoom: 18,
    tileSize: 512,
    zoomOffset: -1
});

/* Mapbox - Satellite */
layers['Mapbox Satellite'] = L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/{style}/tiles/{z}/{x}/{y}?access_token=' + BOX_API_KEY, {
    attribution: '<a href="https://www.mapbox.com/" target="_blank">Mapbox</a>',
    style: 'satellite-streets-v9',
    minZoom: 1,
    maxZoom: 18,
    tileSize: 512,
    zoomOffset: -1
});

/* Mapbox - Light */
layers['Mapbox Light'] = L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/{style}/tiles/{z}/{x}/{y}?access_token=' + BOX_API_KEY, {
    attribution: '<a href="https://www.mapbox.com/" target="_blank">Mapbox</a>',
    style: 'light-v9',
    minZoom: 1,
    maxZoom: 18,
    tileSize: 512,
    zoomOffset: -1
});

/* Mapbox - Dark */
layers['Mapbox Dark'] = L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/{style}/tiles/{z}/{x}/{y}?access_token=' + BOX_API_KEY, {
    attribution: '<a href="https://www.mapbox.com/" target="_blank">Mapbox</a>',
    style: 'dark-v9',
    minZoom: 1,
    maxZoom: 18,
    tileSize: 512,
    zoomOffset: -1
});

/* Mapbox - Outdoor */
layers['Mapbox Outdoor'] = L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/{style}/tiles/{z}/{x}/{y}?access_token=' + BOX_API_KEY, {
    attribution: '<a href="https://www.mapbox.com/" target="_blank">Mapbox</a>',
    style: 'outdoors-v9',
    minZoom: 1,
    maxZoom: 18,
    tileSize: 512,
    zoomOffset: -1
});

/* GoogleMaps Roadmap */
layers['Google Roadmap'] = L.tileLayer('https://mt3.google.com/vt/lyrs=m&hl=en&x={x}&y={y}&z={z}', {
    attribution: '<a href="https://www.google.com/" target="_blank">Google</a>',
    minZoom: 0,
    maxZoom: 22,
    tileSize: 256
});

/* GoogleMaps AltÃ©rÃ© */
layers['Google Maps'] = L.tileLayer('https://mt3.google.com/vt/lyrs=r&hl=en&x={x}&y={y}&z={z}', {
    attribution: '<a href="https://www.google.com/" target="_blank">Google</a>',
    minZoom: 0,
    maxZoom: 22,
    tileSize: 256
});

/* GoogleMaps Satellite */
layers['Google Satellite'] = L.tileLayer('https://mt3.google.com/vt/lyrs=s&hl=en&x={x}&y={y}&z={z}', {
    attribution: '<a href="https://www.google.com/" target="_blank">Google</a>',
    minZoom: 0,
    maxZoom: 22,
    tileSize: 256
});

/* GoogleMaps Terrain */
layers['Google Terrain'] = L.tileLayer('https://mt3.google.com/vt/lyrs=p&hl=en&x={x}&y={y}&z={z}', {
    attribution: '<a href="https://www.google.com/" target="_blank">Google</a>',
    minZoom: 0,
    maxZoom: 22,
    tileSize: 256
});

/* GoogleMaps Hybrid */
layers['Google Hybrid'] = L.tileLayer('https://mt3.google.com/vt/lyrs=y&hl=en&x={x}&y={y}&z={z}', {
    attribution: '<a href="https://www.google.com/" target="_blank">Google</a>',
    minZoom: 0,
    maxZoom: 22,
    tileSize: 256
});

/* GoogleMaps Dark */
layers['Google Dark'] = L.tileLayer('https://mt3.google.com/vt/lyrs=t&hl=en&x={x}&y={y}&z={z}', {
    attribution: '<a href="https://www.google.com/" target="_blank">Google</a>',
    minZoom: 0,
    maxZoom: 22,
    tileSize: 256
});

/* IGN */
layers['IGN France'] = L.tileLayer('https://wxs.ign.fr/{apikey}/geoportail/wmts?service={service}&request={request}&version={version}&tilematrixset={tilemx}&layer={layer}&format={format}&style={style}&tilematrix={z}&tilecol={x}&tilerow={y}', {
    attribution: '<a href="https://www.ign.fr/" target="_blank">IGN</a>',
    apikey: 'z7nbv9c5kktzsezzydsjqs16',
    service: 'WMTS',
    request: 'GetTile',
    version: '1.0.0',
    tilemx: 'PM',
    layer: 'GEOGRAPHICALGRIDSYSTEMS.MAPS',
    format: 'image/jpeg',
    style: 'normal',
    minZoom: 0,
    maxZoom: 18,
    tileSize: 256
});

/* IGN Express Standard */
layers['IGN Express Standard'] = L.tileLayer('https://wxs.ign.fr/{apikey}/geoportail/wmts?service={service}&request={request}&version={version}&tilematrixset={tilemx}&layer={layer}&format={format}&style={style}&tilematrix={z}&tilecol={x}&tilerow={y}', {
    attribution: '<a href="https://www.ign.fr/" target="_blank">IGN</a>',
    apikey: 'z7nbv9c5kktzsezzydsjqs16',
    service: 'WMTS',
    request: 'GetTile',
    version: '1.0.0',
    tilemx: 'PM',
    layer: 'GEOGRAPHICALGRIDSYSTEMS.MAPS.SCAN-EXPRESS.STANDARD',
    format: 'image/jpeg',
    style: 'normal',
    minZoom: 0,
    maxZoom: 18,
    tileSize: 256
});

/* IGN Satellite */
layers['IGN Satellite'] = L.tileLayer('https://wxs.ign.fr/{apikey}/geoportail/wmts?service={service}&request={request}&version={version}&tilematrixset={tilemx}&layer={layer}&format={format}&style={style}&tilematrix={z}&tilecol={x}&tilerow={y}', {
    attribution: '<a href="https://www.ign.fr/" target="_blank">IGN</a>',
    apikey: 'z7nbv9c5kktzsezzydsjqs16',
    service: 'WMTS',
    request: 'GetTile',
    version: '1.0.0',
    tilemx: 'PM',
    layer: 'ORTHOIMAGERY.ORTHOPHOTOS',
    format: 'image/jpeg',
    style: 'normal',
    minZoom: 0,
    maxZoom: 18,
    tileSize: 256
});

/* IGN Plan */
layers['IGN Plan'] = L.tileLayer('https://wxs.ign.fr/{apikey}/geoportail/wmts?service={service}&request={request}&version={version}&tilematrixset={tilemx}&layer={layer}&format={format}&style={style}&tilematrix={z}&tilecol={x}&tilerow={y}', {
    attribution: '<a href="https://www.ign.fr/" target="_blank">IGN</a>',
    apikey: 'z7nbv9c5kktzsezzydsjqs16',
    service: 'WMTS',
    request: 'GetTile',
    version: '1.0.0',
    tilemx: 'PM',
    layer: 'GEOGRAPHICALGRIDSYSTEMS.PLANIGN',
    format: 'image/jpeg',
    style: 'normal',
    minZoom: 0,
    maxZoom: 18,
    tileSize: 256
});

/* IGN Relief */
layers['IGN Relief'] = L.tileLayer('https://wxs.ign.fr/{apikey}/geoportail/wmts?service={service}&request={request}&version={version}&tilematrixset={tilemx}&layer={layer}&format={format}&style={style}&tilematrix={z}&tilecol={x}&tilerow={y}', {
    attribution: '<a href="https://www.ign.fr/" target="_blank">IGN</a>',
    apikey: 'z7nbv9c5kktzsezzydsjqs16',
    service: 'WMTS',
    request: 'GetTile',
    version: '1.0.0',
    tilemx: 'PM',
    layer: 'ELEVATION.SLOPES',
    format: 'image/jpeg',
    style: 'normal',
    minZoom: 6,
    maxZoom: 14,
    tileSize: 256
});

/* IGN Espagne */
layers['IGN España'] = L.tileLayer('https://www.ign.es/wmts/mapa-raster?service={service}&request={request}&version={version}&tilematrixset={tilemx}&layer={layer}&format={format}&style={style}&tilematrix={z}&tilecol={x}&tilerow={y}', {
    attribution: '<a href="https://www.ign.es/" target="_blank">IGN</a>',
    service: 'WMTS',
    request: 'GetTile',
    version: '1.0.0',
    tilemx: 'GoogleMapsCompatible',
    layer: 'MTN',
    format: 'image/jpeg',
    style: 'normal',
    minZoom: 0,
    maxZoom: 18,
    tileSize: 256
});

layers['ICGC Catalunya'] = L.tileLayer('https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/{z}/{x}/{y}.jpeg', {
    maxZoom: 22,  
    wms: true,
    layers: 'topo',
    attribution: 'Institut Cartogràfic i Geològic de Catalunya CC-BY-SA-3'
});

/* Lyrk Map */
layers['Lyrk Map'] = L.tileLayer('https://tiles.lyrk.org/ls/{z}/{x}/{y}?apikey=982c82cc765f42cf950a57de0d891076', {
    attribution: '<a href="https://vectorine.com/" target="_blank">Vectorine</a>',
    maxZoom: 20
});

/* CartoDB BaseMap */
layers['CartoDB BaseMap'] = L.tileLayer('https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png', {
    attribution: '<a href="https://basemaps.com/" target="_blank">Basemaps</a>',
    maxZoom: 20
});

/* WMS Topography */
layers['WMS Topo'] = L.tileLayer.wms('https://ows.mundialis.de/services/service', {
    attribution: '<a href="https://www.mundialis.de/" target="_blank">Mundialis</a>',
    layers: 'TOPO-WMS'
});

/* WMS Place */
layers['WMS Place'] = L.tileLayer.wms('https://ows.mundialis.de/services/service', {
    attribution: '<a href="https://www.mundialis.de/" target="_blank">Mundialis</a>',
    layers: 'OSM-Overlay-WMS'
});

/* WMS Topography & Place */
layers['WMS Hybrid'] = L.tileLayer.wms('https://ows.mundialis.de/services/service', {
    attribution: '<a href="https://www.mundialis.de/" target="_blank">Mundialis</a>',
    layers: 'TOPO-OSM-WMS'
});

/* Stamen Color */
layers['Stamen Color'] = L.tileLayer('http://{s}.tile.stamen.com/{style}/{z}/{x}/{y}.png', {
    attribution: '<a href="https://stamen.com/" target="_blank">Stamen</a>',
    style: 'watercolor',
    minZoom: 1,
    maxZoom: 16
});

/* Stamen Lite */
layers['Stamen Light'] = L.tileLayer('http://{s}.tile.stamen.com/{style}/{z}/{x}/{y}.png', {
    attribution: '<a href="https://stamen.com/" target="_blank">Stamen</a>',
    style: 'toner-lite',
    minZoom: 0,
    maxZoom: 20
});

/* Stamen Toner */
layers['Stamen Toner'] = L.tileLayer('http://{s}.tile.stamen.com/{style}/{z}/{x}/{y}.png', {
    attribution: '<a href="https://stamen.com/" target="_blank">Stamen</a>',
    style: 'toner',
    minZoom: 0,
    maxZoom: 20
});

overlays['Carte des pentes'] = L.tileLayer('https://wxs.ign.fr/{apikey}/geoportail/wmts?service=WMTS&request=GetTile&version=1.0.0&tilematrixset=PM&tilematrix={z}&tilecol={x}&tilerow={y}&layer={layer}&format={format}&style=normal', {
    title: 'IGN pentes',
    apikey: 'z7nbv9c5kktzsezzydsjqs16',
    opacity: 0.5,
    layer: 'GEOGRAPHICALGRIDSYSTEMS.SLOPES.MOUNTAIN',
    minZoom: 2,
    maxZoom: 18,
    apikey: 'altimetrie',
    format: 'image/png',
    attribution: '<a target="_blank" href="https://www.geoportail.gouv.fr/">Geoportail France</a>',
    });

overlays['Chemins de Grande Randonnée'] = L.tileLayer('https://tile.waymarkedtrails.org/hiking/{z}/{x}/{y}.png', {
    minZoom: 0,
	maxZoom: 20,
	attribution: '<a href="https://hiking.waymarkedtrails.org/" target="_blank">Waymarked Trails</a>'
});

overlays['Fond Chemins de cyclisme'] = L.tileLayer('https://tile.waymarkedtrails.org/cycling/{z}/{x}/{y}.png', {
    minZoom: 0,
	maxZoom: 20,
	attribution: '<a href="https://hiking.waymarkedtrails.org/" target="_blank">Waymarked Trails</a>'
});

/* Cadastral Overlay */
overlays['Fond Cadastrales'] = L.tileLayer('https://wxs.ign.fr/{apikey}/geoportail/wmts?layer={layer}&style={style}&tilematrixset={tilemx}&Service={service}&Request={request}&Version={version}&Format={format}&TileMatrix={z}&TileCol={x}&TileRow={y}', {
    attribution: '<a href="https://www.ign.fr/" target="_blank">IGN</a>',
    apikey: 'an7nvfzojv5wa96dsga5nk8w',
    layer: 'CADASTRALPARCELS.PARCELLAIRE_EXPRESS',
    service: 'WMTS',
    request: 'GetTile',
    version: '1.0.0',
    tilemx: 'PM',
    format: 'image/png',
    style: 'PCI%20vecteur',
    minZoom: 6,
    maxZoom: 19,
    tileSize: 256
});



/* Strava Overlay */
/*overlays['PopularitÃ© Strava'] = L.tileLayer('//api.' + HOSTNAME + '/tile/{s}/{z}/{x}/{y}.png', {
    attribution: '<a href="https://strava.com/" target="_blank">Strava</a>',
    minNativeZoom: 3,
    maxNativeZoom: 15
});*/
